FROM composer:latest AS composer
FROM php:7.2
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY src/ /app/salary-test
COPY data/database.sqlite /app/salary-test
COPY config/.env /app/salary-test
WORKDIR /app/salary-test/

# Install mysql and cron
RUN apt-get update && apt-get install -y \
    sqlite3 && composer install
CMD php artisan salary:calculate