Code pipeline:  
1. \App\Console\Commands\SalaryCalculate - console command "artisan salary:calculate"
2. \App\Services\DefaultSalaryCalculator - default calculation engine for employees
3. \App\SalaryRules - rules for salary calculating

Balance of SOLID/KISS:
1. \App\Employee substitutive with \Illuminate\Database\Eloquent\Model to extend
2. \App\SalaryRules - unique logic for \App\Employee

How to run:
Run Dockerfile, read console logs.
