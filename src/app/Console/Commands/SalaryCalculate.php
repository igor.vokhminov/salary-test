<?php

namespace App\Console\Commands;

use App\Contracts\SalaryCalculatorInterface;
use Illuminate\Console\Command;

class SalaryCalculate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'salary:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates salary for this test';

    /**
     * Selected salary calculator service
     *
     * @var SalaryCalculatorInterface;
     */
    private $salary_calculator;

    /**
     * Create a new command instance.
     *
     * @param SalaryCalculatorInterface $salary_calculator
     */
    public function __construct(SalaryCalculatorInterface $salary_calculator)
    {
        parent::__construct();

        $this->salary_calculator = $salary_calculator;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $result = $this->salary_calculator->calculateAll();

        $result->each(function ($item) {
            printf("%s, %.2f\n", $item->name, $item->salary_net);
        });
    }
}
