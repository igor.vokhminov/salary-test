<?php

namespace App\Contracts;

use App\Employee;
use Illuminate\Database\Eloquent\Collection;

interface SalaryCalculatorInterface
{
    public function calculateAll(): Collection;

    public function calculateOne(Employee $unit): Employee;
}
