<?php


namespace App\Contracts;


use App\Employee;

interface SalaryRuleInterface
{
    public function __construct(Employee $employee);

    public function calculate();

    public function setNextRule(SalaryRuleInterface $rule);
}
