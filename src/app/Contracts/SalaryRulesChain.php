<?php


namespace App\Contracts;


trait SalaryRulesChain
{
    /** @var SalaryRuleInterface */
    private $next_rule;

    public function setNextRule(SalaryRuleInterface $rule): SalaryRuleInterface
    {
        $this->next_rule = $rule;

        return $rule;
    }
}
