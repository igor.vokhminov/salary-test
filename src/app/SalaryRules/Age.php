<?php


namespace App\SalaryRules;


use App\Contracts\SalaryRuleInterface;
use App\Contracts\SalaryRulesChain;
use App\Employee;

class Age implements SalaryRuleInterface
{
    use SalaryRulesChain;

    private $employee;
    /** @var SalaryRuleInterface */
    private $next_rule;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function calculate()
    {
        if ($this->employee->age > 50) {
            $this->employee->salary_net *= 1.07;
        }

        if (isset($this->next_rule)) {
            $this->next_rule->calculate();
        }
    }
}
