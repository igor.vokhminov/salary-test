<?php


namespace App\SalaryRules;


use App\Contracts\SalaryRuleInterface;
use App\Contracts\SalaryRulesChain;
use App\Employee;

class RentCar implements SalaryRuleInterface
{
    use SalaryRulesChain;

    /** @var Employee */
    private $employee;
    /** @var SalaryRuleInterface */
    private $next_rule;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function calculate()
    {
        if ($this->employee->rent_car) {
            $this->employee->salary_net -= 500;
        }

        if (isset($this->next_rule)) {
            $this->next_rule->calculate();
        }
    }
}
