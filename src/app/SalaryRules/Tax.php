<?php

namespace App\SalaryRules;


use App\Contracts\SalaryRuleInterface;
use App\Contracts\SalaryRulesChain;
use App\Employee;

class Tax implements SalaryRuleInterface
{
    use SalaryRulesChain;

    private $employee;
    /** @var SalaryRuleInterface */
    private $next_rule;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function calculate()
    {
        if ($this->employee->children_count <= 2) {
            $this->employee->salary_net *= 0.8;
        }

        if (isset($this->next_rule)) {
            $this->next_rule->calculate();
        }
    }
}
