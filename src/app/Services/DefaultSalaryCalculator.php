<?php

namespace App\Services;

use App\Contracts\SalaryCalculatorInterface;
use App\Employee;
use App\SalaryRules\Age;
use App\SalaryRules\ChildrenTax;
use App\SalaryRules\RentCar;
use App\SalaryRules\Tax;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DefaultSalaryCalculator implements SalaryCalculatorInterface
{
    /**
     * Calculates all employees' salary net
     *
     * @return Collection
     */
    public function calculateAll(): Collection
    {
        $employees = Employee::all();

        $employees->each(function ($item) {
            $this->calculateOne($item);
        });

        return $employees;
    }

    /**
     * Calculates specified employee's salary net
     *
     * @param Employee $employee
     * @return Employee
     */
    public function calculateOne(Employee $employee): Employee
    {
        if (!isset($employee->salary_net)) {
            $employee->salary_net = $employee->salary;
        }

        $rule = new Age($employee);
        $rule->setNextRule(new Tax($employee))
            ->setNextRule(new ChildrenTax($employee))
            ->setNextRule(new RentCar($employee));

        $rule->calculate();

        return $employee;
    }
}
