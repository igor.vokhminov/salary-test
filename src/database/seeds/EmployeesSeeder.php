<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'name' => 'Алиса',
            'salary' => 6000,
            'age' => 26,
            'children_count' => 2,
            'rent_car' => false,
        ]);

        DB::table('employees')->insert([
            'name' => 'Владимир',
            'salary' => 4000,
            'age' => 52,
            'children_count' => 0,
            'rent_car' => true,
        ]);

        DB::table('employees')->insert([
            'name' => 'Константин',
            'salary' => 5000,
            'age' => 36,
            'children_count' => 3,
            'rent_car' => true,
        ]);
    }
}
